# Introduction à la simulation de modèles de dynamiques des populations avec Python

Support de cours pour la séance de biomaths du  22/03 - EPU MAM4 - 2020-2021

---

*Simulations informatiques sous Python pour la dynamique des populations :*

- *intégration numérique d'équations différentielles ordinaires*
- *représentation des dynamiques contre le temps et dans le plan de phase*
- *représentation de diagrammes de bifurcation, analyse de l'effet des paramètres*

***

*Rédaction : L. Mailleret, Mars 2022*

## Généralités

### Aides et solutions

Ce support de cours est accompagné d'un [répertoire gitlab](https://forgemia.inra.fr/ludovic.mailleret/biomaths-mam4) qui rassemble plusieurs notebooks jupyter détaillant différents éléments de correction pour chaque partie, quelques aides pour la rédaction de certains éléments de code, ainsi que les figures de résultats que vous devez pouvoir générer. 

Dans la mesure du possible, notamment au début de la séance, n'utilisez pas ces fichiers et essayez de faire les choses par vous-mêmes. Vous apprendrez bien plus efficacement ainsi.

### Début de script

De manière générale, quel que soit le langage, j'ai pour habitude de toujours commencer dans un environnement "propre", avec toutes les variables définies précédemment effacées. Cela permet de ne pas récupérer d'anciennes valeurs de paramètres ou variables issues de précédentes exécutions de script et rend en général le code plus facile à débugger.

Si vous utilisez Spyder, par le menu `Exécution > Configuration par fichier` (ou `Ctrl+F6` par défaut) cocher la case `Effacer toutes les variables avant exécution`. 

Si vous utilisez un notebook Jupyter, entrez la commande magique suivante dans la première cellule :

```python
%reset -f
```

ou alors redémarrez le noyau et exécutez le notebook depuis le début (dans les menus en haut de page). Mefiez vous avec Jupyter de l'ordre dans lequel sont exécutés les cellules et relancez régulièrement l'exécution du notebook.

***

### Modules utilisés

Dans ce cours nous allons nous concentrer sur la simulation numérique de modèles de dynamique de populations à base d'équations différentielles ordinaires. Les simulations se baseront pour l'essentiel sur la fonction `odeint` du module `scipy.integrate` de Python qu'il conviendra d'importer en début de script.

```python
# import de la fonction odeint
from scipy.integrate import odeint
```

Par ailleurs, la manipulation des données numériques et leur représentation graphique utiliseront les modules `numpy` et  `matplotlib.pyplot`, que nous importons aussi en début de script, avec des alias plus courts (et usuels).

```python
# import des modules numpy et matplotlib.pyplot
import numpy as np
import matplotlib.pyplot as plt
```

Si vous utilisez Spyder, je préfère fermer toutes les fenêtres graphiques en début de script, afin d'être sûr que ce qui est visualisé est bien ce qui vient d'être calculé.

```python
# fermeture des graphiques
plt.close('all')      
```

Si vous utilisez Jupyter les graphiques sont inclus dans le notebook et cette étape est inutile.

***

### La fonction `odeint`

La fonction `odeint()` permet d'intégrer une équation différentielle ordinaire. Elle prend pour arguments essentiels :

- une fonction renvoyant la valeur de la dérivée de l'état en fonction de la valeur de l'état `etat`, du temps `t` et de paramètres `params`: 
  
  ```python
  votre_modele(etat, t, params)
  ```

- un `array` de l'état au temps initial `etat0` :
  
  ```python
  etat0 = np.array([x0, y0])
  ```

- un `array`, `tspan`, spécifiant les valeurs du temps auxquelles vous voulez récupérer les valeurs des variables d'état issues de l'intégration numérique :
  
  ```python
  tspan = np.arange(t0, tfin, pas_t)
  ```

Nous allons voir plus spécifiquement sur le modèle de Malthus les différentes syntaxes employées pour définir, intégrer et représenter graphiquement les données simulées. 

Le reste de la séance vous laissera une plus grande part d'improvisation, l'essentiel du code Python dérivant de celui que nous aurons vu ensemble sur l'exemple simple de Malthus.

***

## Populations isolées

### Modèle de Malthus

Le modèle de Malthus, ou à croissance exponentielle, s'écrit pour une population de densité $`x`$ comme nous l'avons vu en cours :

```math
\dot x = (n-m)\ x
```

avec $`n`$ et $`m`$ les taux de natalité et de mortalité, respectivement.

#### Paramétrisation

Le script de simulation du modèle commence par la définition d'une densité initiale de population, que l'on encapsule dans un `array` (ce sera surtout utile en dimension 2 et supérieure) :

```python
# densité initiale de la population
x0 = 0.1

# encapsulation de la densité initiale
etat0_malthus = np.array([x0])
```

ainsi que des paramètres du modèle :

```python
# paramètres du modèle
# taux de natalité
n = 3.0

# taux de mortalité
m = 2.0
```

que l'on encapsule dans un `array` :

```python
# encapsulation des paramètres dans un array
params_malthus = np.array([n, m])
```

On définit finalement les paramètres de l'intervalle de temps sur lequel on souhaite intégrer le modèle :

```python
# definition des paramètres du tspan
t_0 = 0.0
t_fin = 20
pas_t = 0.01
```

qui permettent de définir un `array` partant de `t_0` jusque `t_fin` tous les `pas_t` via la fonction de `numpy` `arange()` :

```python
# définition du tspan
tspan = np.arange(t_0, t_fin, pas_t)
```

#### Définition du modèle

On définit une fonction qui renvoie la dérivée $`\dot x`$ en fonction de la valeur de l'état $`x`$, du temps et des paramètres.

Pour plus de lisibilité du code on désencapsule les paramètres et la valeur des paramètres dans le corps de la fonction en créant des valeurs locales :

```python
# définition du modèle de Malthus
def modele_malthus(etat, t, params): 
    x = etat          # on recupere l'etat
    n, m = params     # on fera bien attention à l'ordre des paramètres défini plus haut
    xdot = (n-m)*x      # on calcule la derivee de l'etat 
    return xdot
```

#### Intégration numérique

On fait un simple appel à la fonction `odeint()` et on en stocke la sortie dans un `array` (qui est *en colonne*) qui représente les valeurs numériques prises par la densité de la population à chaque temps défini dans `tspan` :

```python
# intégration du modèle
int_malthus = odeint(modele_malthus, etat0_malthus, tspan, args=(params_malthus,), hmax=pas_t)
```

L'appel à `odeint()` nécessite :

- en premier argument, la fonction définissant le modèle, ici `modele_malthus`
- en second argument, la condition initiale, ici `etat0_malthus`
- en troisième argument, le tspan, ici `tspan`
- le quatrième argument `args=(params_malthus,)` permet de passer les paramètres `params_malthus` en dernier argument à la fonction définissant le modèle `modele_malthus`
- le paramètre `hmax=pas_t` force le solveur à utiliser `pas_t` comme valeur maximale de pas d'intégration

*NB: d'autres arguments, par exemple de tolérance, peuvent être passés à `odeint()`, je vous renvoie sur l'aide de la fonction, mais les paramètres par défaut sont déjà satisfaisants*

A ce stade, l'exécution du script montre que l'intégration numérique est faite :

```python
In [2]: int_malthus
Out[2]: 
array([[  1.00000000e-01],
       [  1.01005023e-01],
       [  1.02020142e-01],
       ..., 
       [  4.70826724e+07],
       [  4.75558612e+07],
       [  4.80338056e+07]])
```

Le travail restant est essentiellement lié à la représentation graphique des données simulées ; c'est une tâche qui n'en est pas moins importante.

#### Représentation des trajectoires en fonction du temps

Les représentations graphiques utilisent le module `matplotlib.pyplot` que l'on a importé en début de script sous l'alias `plt`.

On commence par créer un objet fenêtre graphique `fig1`  associé à un objet système d'axes `ax1` qui permettra la représentation des données :

```python
# représentation graphique
# création d'une figure, et d'un (ou plusieurs) système d'axes
fig1, ax1 = plt.subplots() 
```

On trace ensuite le résultat de l'intégration numérique `int_malthus` en fonction du temps `tspan` en utilisant la méthode `plot()` de l'objet `ax1` :

```python
# tracé des simulations par rapport au temps
ax1.plot(tspan, int_malthus, color='C0', label='population $x$')
ax1.legend()
```

De nombreuses options peuvent être passées à la méthode `plot()` . Ici j'ai spécifié la couleur de la courbe comme la première couleur de la palette standard de Python. J'ai aussi spécifié un label à la courbe, qui sera affiché par l'exécution de la méthode `legend()` de l'objet `ax1`. 

*NB : Je vous renvoie à l'aide de `plt.subplots` ou `plt.plot` ainsi qu'aux nombreux tutoriels sur internet pour différentes options graphiques.*

Il reste à mettre un peu mieux en forme la figure :

- labellisation des axes :
  
  ```python
  # labellisation des axes
  ax1.set_xlabel('temps')
  ax1.set_ylabel('densité de population')
  ```

- mise en forme d'un titre :
  
  ```python
  # titre de la figure
  fig1.suptitle('Simulation du modèle de Malthus\n $n = {}, m = {}$'.format(n, m), fontsize = '12')
  ```
  
  notez l'utilisation de la méthode `format()` des chaînes de caractères qui permet de renseigner les valeurs des termes accolades par les valeurs du taux de natalité $n$ et du taux de mortalité $`m`$ utilisés dans la simulation. 

- modifier le minimum et le maximum des axes :
  
  ```python
  # modification des bornes des axes
  ax1.set_ylim(bottom=None, top=np.max(int_malthus))
  ```

- ajouter une grille :
  
  ```python
  # ajout d'une grille
  ax1.grid()
  ```

Le cas échéant on pourra vérifier être bien en présence d'une croissance exponentielle en choisissant une échelle logarithmique pour l'axe des ordonnées :

```python
# echelle des ordonnees en log
ax1.set_yscale('log')
ax1.set_ylim(bottom=np.min(int_malthus), top=np.max(int_malthus))
```

mais il convient de recalculer alors les bornes des axes.

#### Résultats de la simulation

![Simulations du modèle de Malthus](./img/Malthus.png)

Petites variations par rapport à ce que nous avons vu plus haut : 

- création d'une figure avec deux subplots en lignes (modification de l'appel à `plt.subplots`), dans lesquels j'ai tracé les solutions en échelle linéaire et logarithmique
- enregistrement de la figure au format `png` pour réutilisation dans d'autres logiciels via la méthode `savefig()` de l'objet `fig1`

*NB : Vous pouvez modifier les paramètres, conditions initiales, et observer les modifications des simulations*

Un script complet rassemblant les différentes étapes que nous avons vu plus haut est disponible sur la page de [corrections](https://forgemia.inra.fr/ludovic.mailleret/biomaths-mam4/-/blob/master/biomaths_mam4_malthus.ipynb).

***

## Populations exploitées

### Prélèvements dans un modèle avec effets Allee forts

L'idée est ici de reprendre l'étude faite en cours en simulant une pêcherie durable soumise à un effort de pêche durable pendant un certain temps, puis en situation de surpêche, avant un retour à la normale. 

Nous simulerons notamment l'effet de la durée de la période de surpêche sur la survie du stock, et illustrerons ces résultats avec des figures représentant la densité de population en fonction du temps, et la densité de population en fonction de l'effort de pêche.

Pour rappel, le modèle s'écrit :

```math
\dot x = r x\left(\frac{x}{K_a}-1\right)\left(1-\frac{x}{K}\right) - E x
```

***

#### Dynamiques par rapport au temps, $`E`$ constant

Nous allons tout d'abord intégrer simplement ce modèle, comme nous l'avons fait plus haut, avec $`r=1,\ K_a=2,\ K=10,\ E=0.2`$ ou $`E=0.85`$ et $`x(0)=10`$.

La simulation n'est pas plus compliquée que dans les exemples précédents. Il en va de même pour la représentation graphique de la densité de population par rapport au temps. 

#### Dynamiques par rapport au temps, $`E`$ varie au cours du temps

Le principe de la simulation est maintenant de faire varier $`E`$ au cours du temps pour étudier les effets de la surpêche. Pour cela :

- créer une fonction `E_varie()`, prenant pour argument le temps $`t`$ et les paramètres $`E_{sust},\ E_{overF},\ T_{sust},\ T_{overF},`$ qui selon la valeur du temps $`t`$ renvoie la valeur $`E_{sust}`$ (effort de pêche durable) ou $`E_{overF}`$ (surpêche) comme décrit dans la figure ci-dessous.
  
  ![effort de peche](./img/effort_peche.png)
  
  *NB : un simple `if` renvoyant la valeur $`E_{sust}`$ ou $`E_{overF}`$ selon la valeur du temps $`t`$ fera l'affaire à ce stade*

- répliquer la figure ci-dessus

- utiliser la fonction `E_varie()` pour simuler le modèle

On choisira pour valeurs de paramètres par exemple :

- $`r=1,\ K_a=2,\ K=10,\ E_{sust}=0.2,\ E_{overF}=0.85`$
- $`T_{sust} = 10`$, et : $`T_{overF }=9.2`$ ou : $`T_{overF} =9`$

L'attendu théorique est un stock qui s'éteint si $`T_{overF}`$ est trop important.

***

#### Représentation dans l'espace $`(E,\ x)`$

On veut ici représenter le diagramme de bifurcation du modèle en fonction de $`E`$ et surajouter dans ce graphique les valeurs de $`x`$ simulées plus haut en fonction de $`E`$.

La partie bifurcation nécessite de tracer le lieu des points d'équilibre dans le plan $`(E, x)`$ en générant un `array` $`x_{plot}`$ entre $`K_a`$ et $`K`$ et, comme dans le cours, en calculant la valeur $`E_{plot}`$ définissant les équilibres et finalement en traçant la courbe définie par $`(E_{plot}, x_{plot})`$

*NB : on n'oubliera pas de tracer $`x=0`$*

Pour la représentation des simulations numériques, on a déjà la valeur de la densité de population $`x`$ au long du temps `tspan`. Nous avons aussi du calculer `E_varie()` au cours du temps pour tracer la figure ci-dessus. Il suffit alors de tracer la valeur de l'état $`x`$ au cours du temps en fonction de la valeur de `E_varie()` au cours du temps.

Un aperçu des résultats attendus et le cas échéant quelques aides à l'écriture du programme sont disponibles dans le fichier la page de [corrections](https://forgemia.inra.fr/ludovic.mailleret/biomaths-mam4/-/blob/master/biomaths_mam4_AE.ipynb), sur laquelle vous pourrez aussi confronter vos résultats aux attendus.

***

### Tordeuse du bourgeon de l'épinette

Nous reprenons l'étude faite en cours sur le modèle de la tordeuse du bourgeon de l'épinette [(Ludwig, Jones et Holling, 1978)](https://jmahaffy.sdsu.edu/courses/f09/math636/lectures/bifurcation_ode/ludwig_ecology_78.pdf). Le modèle que nous allons étudier ici est légèrement différent de celui vu en cours, car nous allons expliciter la population d'oiseaux prédateurs. Avec $`x`$ la densité de tordeuses et $`y`$ la densité d'oiseaux, le modèle s'écrit :

```math
\dot x =rx\left(1-\frac{x}{K}\right) - \frac{\alpha x^2}{h^2+x^2}\ y
```

$`\alpha`$ est le nombre maximal de chenilles que les oiseaux peuvent consommer *per capita* ; les autres paramètres ont été vus en cours. 

Nous allons dans un premier temps étudier le modèle à population d'oiseaux constante, représenter plusieurs dynamiques en fonction du temps et tracer le diagramme de bifurcations correspondant dans l'espace $`(y,x)`$. 

Dans un second temps, nous supposerons que la taille de la population d'oiseaux varie lentement, en fonction des captures de chenilles et d'un taux de mortalité. Nous observerons une situation d’enchaînement de bifurcations dynamiques qui crée un cycle d'hysteresis que nous représenterons en fonction du temps et dans l'espace $`(y,x)`$.

***

#### Taille de population d'oiseaux constante

On choisira pour valeurs de paramètres par exemple :

- $`r=5,\ K=10,\ h=0.5,\ \alpha = 1`$ et la densité de population d'oiseaux $`y=7`$

La simulation ne pose pas de difficulté particulière par rapport à ce qu'on a fait précédemment (si ce n'est de se rappeler que le carré en Python s'écrit `**2`). Il peut être intéressant de représenter différentes dynamiques afin d'illustrer la multi-stabilité dans ce modèle.

Le tracé du diagramme de bifurcation se fait par une méthode très similaire au modèle sur la pêche via un array `x_plot` et le calcul des $y$ qui correspondent aux valeurs des équilibres.

***

#### Taille de population d'oiseaux variable

Lorsque l'on prend en compte que la taille de la population d'oiseaux varie au cours du temps en fonction des captures, le modèle est directement en 2 dimensions. En supposant que la dynamique des oiseaux est lente par rapport à celle des insectes, on peut cependant déduire le comportement en projetant la dynamique des tordeuses à l'équilibre et en étudiant la dynamique résultante sur les oiseaux (approche dite "lent-rapide"). Comme ici, selon la taille de la population d'oiseaux, différents équilibres peuvent exister et être globalement stable, cela va donner un lieu a un comportement dynamique intéressant que l'on pourra entièrement appréhender sur le diagramme de bifurcation tracé précédemment.

Le modèle prend maintenant la forme :

```math
\left\{
\begin{array}{l}
\displaystyle \dot x = rx\left(1-\frac{x}{K}\right) - \frac{\alpha x^2}{h^2+x^2}\ y \\[.2cm]
\displaystyle \dot y = \varepsilon \left(\frac{n \alpha x^2}{h^2+x^2}\ y -m y\right)
\end{array}
\right.
```

avec $`0<\varepsilon\ll 1`$, et $`n`$ et $`m`$ les taux associés à la natalité et à la mortalité des prédateurs. 

En pratique fixons $`\varepsilon=0.01`$, et les paramètres $`n=5, \ m=3`$.  

La seule difficulté ici est qu'il ne s'agit plus d'intégrer une équation différentielle de dimension 1, mais un système de deux équations différentielles. 

Pour cela, il faut fournir à `odeint()` un modèle qui renvoie les valeurs des dérivées de l'état $`\dot{x}`$ et $`\dot y`$ sous forme d'un array ligne de type `[xdot, ydot]` . `odeint()` renvoie ensuite les valeurs de l'intégration numérique sous forme d'un tableau en dimension 2 dont la $`k`$-ième colonne représente la valeur de la $`k`$-ième variable le long de `tspan`.

Une fois l'intégration numérique effectuée vous tracerez : 

- la densité de population de tordeuses en fonction du temps
- la densité de population de tordeuses en fonction de la densité de population d'oiseaux, au cours du temps, surimposée au diagramme de bifurcation du modèle avec la population d'oiseaux constante.

Vous pouvez vous rapporter à la page de [corrections](https://forgemia.inra.fr/ludovic.mailleret/biomaths-mam4/-/blob/master/biomaths_mam4_tordeuse.ipynb) pour confronter vos résultats aux attendus.

***

## Populations en interactions

### Le modèle proies-prédateurs de Lotka-Volterra

#### Dynamiques et plan de phase

Nous considérons le modèle de dynamique de populations (classique) de Lotka et Volterra.

```math
\left\{\begin{array}{l}
\dot x = rx - c xy\\
\dot y = bxy - m y
\end{array}\right.
```

Nous allons simuler ce modèle pour $`r=1,c=1,b=1,m=1`$, représenter les dynamiques de populations en fonction du temps et dans le plan de phase 

$`(x,y)`$



. Cette partie est calquée sur ce que nous avons fait précédemment pour le modèle de tordeuse avec population d'oiseaux variable.

Les subtilités concernent la représentation dans le plan de phase. Pour cela nous calculerons les isoclines nulles et représenterons les équilibres, et la trajectoire simulée.

Nous utiliserons par ailleurs les méthodes `streamplot()` et `quiver()` des systèmes d'axes pour représenter l'orientation du champs de vecteur et quelques échantillons de trajectoires approximées. Ces deux méthodes calculent le champs de vecteur ou une approximation des trajectoires sur une grille dans le plan $`(x,y)`$. Il convient d'abord de définir cette grille :

```python
# définition de l'échantillonnage selon $x$ et $y$
x_grid = np.linspace(0, 3.0, 10)   # au passage on change un peu de np.arange()
y_grid = np.linspace(0, 3.0, 10)

# grille X,Y selon x_grid et y_grid
X, Y = np.meshgrid(x_grid, y_grid)
```

Puis nous calculons les valeurs des dérivées sur cette grille :

```python
# dérivées sur la grille
dx, dy = modele_LV([X, Y], 0, params_LV)
```

Ainsi, nous pouvons tracer le champs de vecteurs :

```python
# tracé du champs de vecteurs
# Attention à l'option angle ='xy' les fleches sont tracees avec orientation en unité naturelle de l'ecran et pas en unité naturelle de la figure
ax2.quiver(X, Y, dx, dy, angles = 'xy', color = 'grey', width = .002)
```

et les échantillons de trajectoires :

```python
# tracé des échantillons de trajectoires
ax2.streamplot(X, Y, dx, dy, density = 0.4, maxlength = 0.25, color = "purple")
```

#### Intégrale première

Finalement, nous représentons l'intégrale première (quantité conservée au cours du temps dans ce système) en fonction du temps, puis dans un graphique en 3D en fonction de $`x`$ et de $`y`$, ainsi qu'une trajectoire du système dans cet espace. La quantité conservée au cours du temps dans le modèle de Lotka Volterra est la suivante:

```math
H(x,y) = b\ x-m\ln(x) + c\ y- r\ln(y)
```

Nous commençons par définir cette fonction, ce qui devrait être assez direct:

```python
# définition de la fonction conservée au cours du temps dans le modèle de Lotka Volterra
def int_premiere(etat, params):
    x, y = etat                               # recupere les variables d'etat
    r_l, c_l, b_l, m_l = params               # recupere les parametres 
    H_xy = -r_l*np.log(y) + c_l*y - m_l*np.log(x) + b_l*x  # calcule la valeur de la fonction
    return H_xy
```

On peut facilement représenter l'évolution de cette fonction au cours du temps en l'évaluant le long de la trajectoire simulée plus haut et en la traçant en fonction de `tspan`.

*Pour cette fin de partie vous pouvez vous aider du fichier de [correction](https://forgemia.inra.fr/ludovic.mailleret/biomaths-mam4/-/blob/master/biomaths_mam4_LV.ipynb), mais l'essentiel des commandes utiles est repris ci-dessous.*

 La partie 3D est plus technique et utilise différentes fonctions spécifiques aux graphiques 3D, à commencer par:

- La création d'un système d'axes en 3D:

```python
# création d'une figure, et d'un système d'axe un peu particulier puisqu'il est en 3D
fig4, ax4 = plt.subplots(1, 1, subplot_kw={"projection": "3d"}, figsize=(14, 8)) 
```

- Le calcul d'une fonction de deux variables à valeurs dans $\mathbb{R}$ sur une grille (qui est cependant similaire à ce que l'on a fait plus haut pour la représentation du champs de vecteur):

```python
# définition de l'échantillonnage selon $x$ et $y$
x_grid = np.linspace(0.15, 3.0, 30)   # au passage on change un peu de np.arange()
y_grid = np.linspace(0.15, 3.0, 30)

# grille X,Y selon x_grid et y_grid
X, Y = np.meshgrid(x_grid, y_grid)

# calcul de la fonction conservee sur la grille
int_premiere_grid = int_premiere([X, Y], params_LV)
```

- La représentation de cette fonction en fonction de $`x`$ et $`y`$ :

```python
# nous tracons l'integrale première sur la grille
ax4.plot_surface(X, Y, int_premiere_grid, cmap=cm.viridis, antialiased=True, alpha =.7)

# réglage de l'angle de vision en fonction de l'élévation et de l'azimut
ax4.view_init(elev=10, azim= 30)

# labellisation des axes
ax4.set_xlabel('Proies $x$', fontsize='16')
ax4.set_ylabel('Prédateurs $y$', fontsize='16')
ax4.set_zlabel("intégrale première", fontsize='16')

# titre de la figure
fig4.suptitle("Intégrale première\n modèle de Lotka Volterra", va='top', fontsize='18')
```

Finalement nous pouvons surimposer sur cette figure le plan correspondant à la valeur initiale de l'intégrale première en fonction de $`x`$ et $`y`$, ainsi que l'évolution de cette quantité au cours du temps le long de la trajectoire simulée:

```python
# tracé du plan correspondant a la valeur initiale de l'intégrale première
ax4.plot_surface(X, Y, np.ones_like(int_premiere_grid)*int_premiere([x0, y0], params_LV), 
                 antialiased=True, alpha =.3)

# tracé de la valeur de l'intégrale première le long de la trajectoire simulée
ax4.plot(int_LV[:,0], int_LV[:,1], int_premiere([int_LV[:,0], int_LV[:,1]], params_LV), 
         color = "red", linewidth = 3)

# affichage de la figure 4
display(fig4)
```

### Le modèle proies-prédateurs de Rosenzweig-MacArthur

Nous considérons le modèle de dynamique de populations attribué à Rosenzweig et MacArthur (voir [Rosenzweig & MacArthur (1963)](http://www.jstor.com/stable/2458702), [Turchin (2003)](http://www.jstor.com/stable/2458702), [Smith (2008)](https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.710.95&rep=rep1&type=pdf)).

```math
\left\{\begin{array}{l}
\dot x = \displaystyle rx\left(1-\frac{x}{K}\right) - c \frac{x}{h+x} y\\
\dot y = b\displaystyle \frac{x}{h+x} y - m y
\end{array}\right.
```

avec $`x`$ et $`y`$ les densités de proies et prédateurs, respectivement.

#### Simulation des dynamiques et plan de phase

La simulation et la représentation graphique est vraiment dans la droite ligne de ce qui a été fait plus haut pour le modèle de Lotka Volterra.

On prendra comme valeurs de paramètres :

```math
r=1,\ K=10,\ c= 1,\ h=2,\ b=2,\ m=1.
```

Pour ces valeurs, il existe un cycle limite attractif. Vous pouvez diminuer $`K`$ pour voir l'effondrement du cycle en un équilibre positif asymptotiquement stable, voire l'extinction des prédateurs.

#### Diagramme de bifurcations $`y_\infty`$ en fonction de $`K`$

Il y a de nombreuses manières de faire des diagrammes de bifurcations en fonction de paramètres, notamment :

- directement par l'analyse, comme nous l'avons vu en cours (quand c'est possible),
- en suivant numériquement les équilibres (ou des invariants) et en caractérisant numériquement les changements de stabilité portant la signature des bifurcations (e.g [Blyth *et alii*, 2020](https://arxiv.org/pdf/2008.05226.pdf)),
- par force brute numérique, en simulant le système jusqu'à atteindre les états asymptotiques.

C'est cette dernière méthode que nous allons utiliser ici, en combinaison avec l'approche analytique. Ce n'est pas la méthode la plus élégante, mais elle permettra d'obtenir le diagramme recherché.

Le principe de l’algorithme est le suivant :

- fixer une valeur de paramètre
- pour cette valeur, simuler pendant "longtemps" le système dynamique
- depuis la valeur de l'état atteinte, relancer une simulation et récupérer les extrema de celle-ci
  - cela permet de capturer les équilibres asymptotiquement stables, ainsi que les maxima et minima des trajectoires périodiques lorsqu'il y a des cycles limites attractifs (et c'est bien l'attendu dans le modèle de Rosenzweig - MacArthur)
- enregistrer ces valeurs
- incrémenter le paramètre et recommencer

Une fois l'espace de paramètres exploré au travers de cette boucle, on pourra tracer les valeurs asymptotiques calculées en fonction du paramètre, et compléter la figure avec les équilibres instables calculés à la main.

En pratique on réservera l'algorithme présenté ci-dessus au calcul des extrema du cycle limite, et on tracera directement les équilibres que nous savons caractériser sans simulation.

Il est fort possible que nous manquions de temps pour faire cette partie, je vous invite à consulter [la correction](https://forgemia.inra.fr/ludovic.mailleret/biomaths-mam4/-/blob/master/biomaths_mam4_RMA.ipynb).
