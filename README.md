# Dynamiques de populations avec Python

Fichiers supports pour le cours de biomathématique - MAM4 EPU Nice Sophia

Séance de simulations numériques avec Python.

Les fichiers comprennent :
- un [support de cours](./simulations_python.md) d'initiation à la simulation de dynamiques de populations avec Python
- différents fichiers Python (notebooks .ipynb) pour la simulation et la représentation de différents modèles
